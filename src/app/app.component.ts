import { Component } from '@angular/core';
import { ApiService } from './services/api.service';
import { cloneDeep } from 'lodash';
import { CalculationEngineService } from './services/calculationEngine.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  initialData!: boolean[][];
  currentData!: boolean[][];
  initialRows: number = 0;
  initialColumns: number = 0;
  currentRows: number = 0;
  currentColumns: number = 0;
  newData!: boolean[][];
  isStarted: boolean = false;
  isCustomModeEnabled: boolean = false;
  calculateFunctionInterval: any;

  constructor(private apiService: ApiService, private calculationEngineService : CalculationEngineService) {

  }

  ngOnInit(): void {
    this.apiService.get("initialData").subscribe((data: any) => {
      this.initialData = cloneDeep(data);
      this.currentData = cloneDeep(data);
      this.initialRows = data.length;
      this.initialColumns = data[0].length;
      this.currentRows = data.length;
      this.currentColumns = data[0].length;
    })

  }

  start() {
    this.isStarted = true;
    this.calculateFunctionInterval = setInterval(this.calculationEngineService.executeCalculation.bind(this.calculationEngineService, this.currentData), 200);
  }

  pause() {
    this.isStarted = false;
    clearInterval(this.calculateFunctionInterval);
  }

  loadDefault() {
    this.pause();
    this.currentData = cloneDeep(this.initialData);
    this.currentRows = this.initialRows;
    this.currentColumns = this.initialColumns;
  }
  enableCustomMode() {
    this.pause();
    clearInterval(this.calculateFunctionInterval);
    this.isCustomModeEnabled = true;
  }
  disableCustomMode() {
    this.isCustomModeEnabled = false;
  }

  clearBoard() {
    this.currentData = this.calculationEngineService.instantiateMultidimentionalArray(this.currentRows, this.currentColumns);
  }

}
