import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material/material.module';



@NgModule({
	declarations: [
	],
	imports: [
		FlexLayoutModule,
		BrowserModule,
		BrowserAnimationsModule,
		LayoutModule,
		MaterialModule
	],
	exports:[
	],
	bootstrap: []
})
export class CommonsModule { }
