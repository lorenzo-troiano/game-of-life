
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculationEngineService {

  
  constructor() { }

  executeCalculation(data : any) {
    let rows = data.length;
    let columns = data[0].length;
    let newData = this.instantiateMultidimentionalArray(rows, columns);
    
    //calculate new values
    for(var row = 0; row < data.length; row++) {      
      for(var column = 0; column < data[row].length; column++) {
        this.calculateValue(data, newData, row, column);
      }
    }
    //substitute new values with old values
    for(var row = 0; row < newData.length; row++) {      
      for(var column = 0; column < newData[row].length; column++) {
        data[row][column] = newData[row][column];
      }
    }
  }

  calculateValue(data: any, newData : any, row: number, column: number) {

    let isCurrentCellAlive = data[row][column];

    let isBorderTop = row === 0;
    let isBorderLeft = column === 0;
    let isBorderRight = column === data[row].length-1;
    let isBorderBottom = row === data.length-1;

    let nordCellValue = !isBorderTop  ? data[row-1][column] : false;
    let westCellValue = !isBorderLeft ? data[row][column-1] : false;
    let eastCellValue = !isBorderRight ? data[row][column+1] : false;
    let southCellValue = !isBorderBottom ? data[row+1][column] : false;
    let nordEastCellValue = !isBorderTop && !isBorderRight ? data[row-1][column+1] : false;
    let nordWestCellValue = !isBorderTop && !isBorderLeft ?  data[row-1][column-1] : false;
    let southEastCellValue = !isBorderBottom && !isBorderRight ? data[row+1][column+1] : false;
    let southWestCellValue = !isBorderBottom && !isBorderLeft ?  data[row+1][column-1] : false;
    const arr = [nordCellValue, westCellValue, eastCellValue, southCellValue, nordEastCellValue, nordWestCellValue, southEastCellValue, southWestCellValue];
    const liveNeighborsCount = arr.filter(Boolean).length; //liveNeighborsCounts number of booleans

    let isCurrentCellStillAlive = isCurrentCellAlive;//default if no if activate
    if(isCurrentCellAlive){
      if(liveNeighborsCount<2){        
        isCurrentCellStillAlive = false;
      }
      if(liveNeighborsCount === 2 || liveNeighborsCount === 3 ){
        isCurrentCellStillAlive = true;
      }
      if(liveNeighborsCount>3){        
        isCurrentCellStillAlive = false;
      }
    }else if(liveNeighborsCount === 3){
      isCurrentCellStillAlive = true;
    }

    newData[row][column] = isCurrentCellStillAlive;
  }

    
  instantiateMultidimentionalArray (rows: number, columns: number) : any[][]{
    let data = new Array(rows);
    for(var i = 0; i<rows; i++){
      data[i] = new Array(columns)
    }
    return data;
  }

}
